package cc.xhal.platform.sys.serv.impl;

import cc.xhal.platform.core.serv.impl.BaseCurdServiceImpl;
import cc.xhal.platform.sys.serv.UserService;
import org.springframework.stereotype.Service;

/**
 * 功能说明：用户管理 - 业务处理层 - 实现
 *
 * @author hal 2021/7/30 17:00
 * @version 1.0
 */
@Service("userService")
public class UserServiceImpl extends BaseCurdServiceImpl implements UserService {


}
