package cc.xhal.platform.sys.serv;

import cc.xhal.platform.core.serv.BaseCurdService;

/**
 * 功能说明：用户管理 业务处理层 - 接口
 *
 * @author hal 2021/7/30 16:59
 * @version 1.0
 */
public interface UserService extends BaseCurdService {

}
