package cc.xhal.platform.sys.web;

import cc.xhal.platform.core.web.BaseController;
import cc.xhal.platform.sys.domain.bo.User;
import cc.xhal.platform.sys.serv.UserService;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 功能说明：用户管理 - 控制层
 *
 * @author hal 2021/7/30 14:31
 * @version 1.0
 */
@Controller
@RequestMapping("/sys/user")
public class UserController extends BaseController {

    UserService userService;

    /**
     * 获取分页数据
     *
     * @param pageNum  当前页
     * @param pageSize 分页记录大小
     * @return String
     */
    @ResponseBody
    @PostMapping("/listPage")
    public String listPage(Integer pageNum, Integer pageSize) {

        List<User> users = userService.getSuidRich().select(new User(), 0, 100);

        return JSON.toJSONString(users);
    }

    /**
     * 插入记录
     *
     * @param user User
     */
    @ResponseBody
    @PostMapping("/insert")
    public String insert(User user) {
        System.out.println("User Object: " + JSON.toJSONString(user));
        int num = userService.getSuidRich().insert(user);
        System.out.println("insert num: " + num);
        return "success";
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
