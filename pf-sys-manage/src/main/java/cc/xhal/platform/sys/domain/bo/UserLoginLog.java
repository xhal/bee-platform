package cc.xhal.platform.sys.domain.bo;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统用户登陆日志表(TSysUserLoginLog)实体类
 *
 * @author hal@xhal.net
 * @since 2021-07-30 16:42:14
 */
public class UserLoginLog implements Serializable {

    private static final long serialVersionUID = -76326904581459860L;

    /**
     * 表ID
     */
    private Long tid;
    /**
     * 用户表TID
     */
    private Long userId;
    /**
     * 登陆次数
     */
    private Integer loginCount;
    /**
     * 登陆时间
     */
    private Date loginTime;
    /**
     * 登陆IP
     */
    private String loginIp;
    /**
     * 系统类型(Window/ Linux/ MacOs /Andiord/ ISO等等)
     */
    private String systemType;
    /**
     * 浏览器类型（Chrome/IE/WindowsEdge等等）
     */
    private String browserType;
    /**
     * 登陆状态标识（0-正常登陆，1-密码错误
     */
    private Integer statusFlag;


    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getSystemType() {
        return systemType;
    }

    public void setSystemType(String systemType) {
        this.systemType = systemType;
    }

    public String getBrowserType() {
        return browserType;
    }

    public void setBrowserType(String browserType) {
        this.browserType = browserType;
    }

    public Integer getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(Integer statusFlag) {
        this.statusFlag = statusFlag;
    }

}
