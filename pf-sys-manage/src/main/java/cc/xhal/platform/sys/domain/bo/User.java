package cc.xhal.platform.sys.domain.bo;

import cc.xhal.platform.core.domain.bo.BaseLongTidInfoEntity;
import org.teasoft.bee.osql.annotation.Entity;

import java.util.Date;

/**
 * 系统用户信息表(TSysUser)实体类
 *
 * @author hal@xhal.net
 * @since 2021-07-30 16:42:05
 */
@Entity("t_sys_user")
//@Table("t_sys_user")
public class User extends BaseLongTidInfoEntity {

    public static final long serialVersionUID = -21458314197573534L;

    /**
     * 用户账号
     */
    private String userAccount;
    /**
     * 密码
     */
    private String userPswd;
    /**
     * 密码加密盐值
     */
    private String pswdSalt;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 性别（0-女，1-男）
     */
    private Integer gender;
    /**
     * 生日
     */
    private Object birthday;
    /**
     * 电子邮箱地址
     */
    private String email;
    /**
     * 手机号码
     */
    private String cellPhoneNumber;
    /**
     * 座机号
     */
    private String phoneNumber;
    /**
     * 登陆次数
     */
    private Integer loginCount;
    /**
     * 最后登陆时间
     */
    private Date lastLoginTime;
    /**
     * 最后登陆IP
     */
    private String lastLoginIp;
    /**
     * 状态标识（0-正常，1-限制登陆，2-禁用
     */
    private Integer statusFlag;

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserPswd() {
        return userPswd;
    }

    public void setUserPswd(String userPswd) {
        this.userPswd = userPswd;
    }

    public String getPswdSalt() {
        return pswdSalt;
    }

    public void setPswdSalt(String pswdSalt) {
        this.pswdSalt = pswdSalt;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Object getBirthday() {
        return birthday;
    }

    public void setBirthday(Object birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public Integer getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(Integer statusFlag) {
        this.statusFlag = statusFlag;
    }

}
