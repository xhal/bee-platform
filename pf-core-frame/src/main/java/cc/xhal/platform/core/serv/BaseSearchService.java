package cc.xhal.platform.core.serv;

import org.teasoft.bee.osql.Suid;

/**
 * 功能说明：基础查询 业务实现层 - 接口
 *
 * @author hal 2021/7/27 23:45
 * @version 1.0
 */
public interface BaseSearchService extends BaseService {

    /**
     * 获取 Bee ObjSQLService (数据库操作)对象
     *
     * @return ObjSQLService
     */
    Suid getSuid();

}
