package cc.xhal.platform.core.domain.bo;

import java.util.Date;

/**
 * <pre>
 * 功能说明：实体基类
 *    - 表记录主键为 Tid 且为 Long 类型
 *    - 通用信息字段： deleted 逻辑删除标识（0-未删除，等于 tid 则为删除）
 *    - 通用信息字段： create_by   创建人-账号
 *    - 通用信息字段： create_name 创建人-真实姓名
 *    - 通用信息字段： create_time 创建时间
 *    - 通用信息字段： update_by   最后修改人-账号
 *    - 通用信息字段： update_name 最后修改人-真实姓名
 *    - 通用信息字段： update_time 最后修改时间
 *
 *
 * </pre>
 *
 * @author hal 2021/7/30 16:51
 * @version 1.0
 */
public abstract class BaseLongTidInfoEntity extends BaseLongTidEntity {


    /**
     * 逻辑删除（0-未删除，等于记录TID则代表已删除）
     */
    private Long deleted;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建人名称
     */
    private String createName;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改人名称
     */
    private String updateName;
    /**
     * 修改时间
     */
    private Date updateTime;


    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
