package cc.xhal.platform.core.serv.impl;

import cc.xhal.platform.core.serv.BaseCurdService;
import org.teasoft.bee.osql.Suid;
import org.teasoft.bee.osql.SuidRich;
import org.teasoft.honey.osql.core.BeeFactory;

/**
 * 功能说明：增删改查 - 基础业务层实现类
 *
 * @author hal 2021/7/28 21:31
 * @version 1.0
 */
public class BaseCurdServiceImpl extends BaseSearchServiceImpl implements BaseCurdService {

    /**
     * Bee Suid(数据库 - 拓展操作)对象
     */
    private SuidRich suidRich;

    @Override
    public Suid getSuid() {
        return getSuidRich();
    }

    @Override
    public SuidRich getSuidRich() {
        if (null == suidRich) {
            synchronized (this) {
                if (null == suidRich) {
                    suidRich = BeeFactory.getHoneyFactory().getSuidRich();
                }
            }
        }
        return suidRich;
    }
}
