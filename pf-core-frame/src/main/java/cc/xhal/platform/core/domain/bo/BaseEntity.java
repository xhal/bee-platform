package cc.xhal.platform.core.domain.bo;

import java.io.Serializable;

/**
 * 功能说明：基础实体类
 *
 * @author hal 2021/7/27 23:08
 * @version 1.0
 */
public interface BaseEntity<ID extends Serializable> extends Serializable {


    /**
     * 获取ID
     *
     * @return ID
     */
    ID getTid();

}
