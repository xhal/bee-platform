package cc.xhal.platform.core.domain.bo;

import java.util.Objects;

/**
 * 功能说明：实体基类 - 表记录主键为 Tid 且为 Long 类型
 *
 * @author hal 2021/7/30 16:45
 * @version 1.0
 */
public abstract class BaseLongTidEntity implements BaseEntity<Long> {

    /**
     * 表记录主键 TID
     */
    private Long tid;

    @Override
    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || null == tid || getClass() != o.getClass()) return false;

        BaseLongTidEntity that = (BaseLongTidEntity) o;

        return that.tid != null && tid.equals(that.tid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tid);
    }
}
