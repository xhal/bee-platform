package cc.xhal.platform.core.serv.impl;

import cc.xhal.platform.core.serv.BaseSearchService;
import org.teasoft.bee.osql.Suid;
import org.teasoft.honey.osql.core.BeeFactory;

/**
 * 功能说明：基础查询 - 抽象实现类
 *
 * @author hal 2021/7/27 23:16
 * @version 1.0
 */
public abstract class BaseSearchServiceImpl implements BaseSearchService {

    /**
     * Bee Suid(数据库操作)对象
     */
    private Suid suid;

    @Override
    public Suid getSuid() {
        if (null == suid) {
            synchronized (this) {
                if (null == suid) {
                    suid = BeeFactory.getHoneyFactory().getSuid();
                }
            }
        }
        return suid;
    }
}
