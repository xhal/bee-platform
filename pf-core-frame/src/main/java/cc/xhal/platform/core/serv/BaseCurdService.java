package cc.xhal.platform.core.serv;

import org.teasoft.bee.osql.SuidRich;

/**
 * 功能说明：基础增删改查 业务实现层 - 接口
 *
 * @author hal 2021/7/27 23:45
 * @version 1.0
 */
public interface BaseCurdService extends BaseSearchService {

    /**
     * Bee Suid(数据库 - 拓展操作)对象
     *
     * @return SuidRich
     */
    SuidRich getSuidRich();

}
