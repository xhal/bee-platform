package cc.xhal.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestUtil {

    /**
     * 数据分组
     *
     * @param planSeq
     * @return
     */
    public static List<IndexGroup> toGroup(int[] planSeq) {
        List<IndexGroup> groupList = new ArrayList<>();
        int minIdx = -1;
        int maxIdx = -1;
        for (int i = 0; i < planSeq.length; i++) {
            int val = planSeq[i];
            if (val > 0) {
                if (minIdx < 0) {
                    minIdx = i;
                }
                maxIdx = i;
            }
            if (val <= 0 || i + 1 == planSeq.length) {
                if (minIdx >= 0) {
                    groupList.add(new IndexGroup(minIdx, maxIdx));
                }
                minIdx = -1;
            }
        }

        return groupList;
    }

    /**
     * 分析组合方案
     *
     * @param groupList
     * @return
     */
    public static List<List<IndexGroup>> analysisScheme(List<IndexGroup> groupList) {
        int size = groupList.size();
        List<List<IndexGroup>> schemeList = new ArrayList<>();
        // 组合数，最少两个
        while (size > 1) {
            System.out.println("当前要求" + size + "组连续");
            for (int i = 0; i < groupList.size(); i++) {
                List<IndexGroup> subList = new ArrayList<>();
                for (int j = i; j < groupList.size(); j++) {
                    IndexGroup indexGroup = groupList.get(j);
                    subList.add(indexGroup);
                    if (subList.size() == size) {
                        schemeList.add(subList);
                        break;
                    }
                }
            }
            size--;
        }

        return schemeList;
    }

    public static void swapOneByOne(int[] arr, int minIdx, int maxIdx) {

        for (int i = minIdx; i < maxIdx; i++) {
            swap(arr, i, i + 1);
        }
    }
    public static void swap(int[] arr, int i, int j) {
        if (i == j) {
            return;
        }
        arr[i] = arr[i] ^ arr[j];
        arr[j] = arr[i] ^ arr[j];
        arr[i] = arr[i] ^ arr[j];
    }

    public static List<SchemeVO> convert2SchemeVO(List<IndexGroup> groupList, int[] originalPlanSeq) {
        // TODO: 暂只处理，其中一个不动的场景； 不处理多个移动的方案

        List<SchemeVO> list = new ArrayList<>();

        for (int i = 0, size = groupList.size(); i < size; i++) {
            IndexGroup group = groupList.get(i);
            System.out.println("当前不动的索引位置： " + i + "  " + group);

            int groupMinIdx = group.getMinIdx();
            int groupMaxIdx = group.getMaxIdx();

            SchemeVO schemeVO = new SchemeVO("", originalPlanSeq);

            int[] optimizedPlanSeq = Arrays.copyOf(originalPlanSeq, originalPlanSeq.length);
            // 记录往后移动的操作
            List<MoveIndexVO> moveIndexVOS = new ArrayList<>();

            // 往前处理
            for (int j = i - 1; j >= 0; j--) {
                IndexGroup group1 = groupList.get(j);
                int moveMinIdx = group1.getMinIdx();
                int moveMaxIdx = group1.getMaxIdx();
                int moveTargetIdx = groupMinIdx - 1;
                groupMinIdx = groupMinIdx - (group1.getMaxIdx() - group1.getMinIdx() + 1);

                System.out.println("P-Min:" + moveMinIdx + " Max:" + moveMaxIdx + " Target:" + moveTargetIdx);
                // 生成移动步骤记录
                for (int k = 0, ksize = moveMaxIdx - moveMinIdx + 1; k < ksize; k++) {
                    int srcIdx = moveMaxIdx - k;
                    int targetIdx = moveTargetIdx - k;
                    moveIndexVOS.add(new MoveIndexVO(srcIdx, targetIdx));
                    System.out.println("MS: " + srcIdx + "   --  MT: " + targetIdx);
                    swap(optimizedPlanSeq, srcIdx, targetIdx);
                }
            }

            // 往后处理
            for (int j = i + 1; j < size; j++) {
                IndexGroup group1 = groupList.get(j);

                int moveMinIdx = groupMaxIdx + 1;
                int moveMaxIdx = group1.getMinIdx() - 1;
                int moveTargetIdx = group1.getMaxIdx();
                groupMaxIdx = groupMaxIdx + (group1.getMaxIdx() - group1.getMinIdx() + 1);

                System.out.println("N-Min:" + moveMinIdx + " Max:" + moveMaxIdx + " Target:" + moveTargetIdx);
                // 生成移动步骤记录
                for (int k = 0, ksize = moveMaxIdx - moveMinIdx + 1; k < ksize; k++) {
                    int srcIdx = moveMaxIdx - k;
                    int targetIdx = moveTargetIdx - k;
                    moveIndexVOS.add(new MoveIndexVO(srcIdx, targetIdx));
                    System.out.println("MS: " + srcIdx + "   --  MT: " + targetIdx);
                    swapOneByOne(optimizedPlanSeq, srcIdx, targetIdx);
                }
            }
            System.out.println("优化后： " + Arrays.toString(optimizedPlanSeq));
            schemeVO.setOptimizedPlanSeq(optimizedPlanSeq);
            schemeVO.setMoveList(moveIndexVOS);
            list.add(schemeVO);
        }
        return list;
    }

    public static void main(String[] args) {
        //                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
        //int[] arr = new int[]{0, 1, 2, 3, 0, 0, 0, 4, 5, 0,  6,  0,  7,  8,  0,  9};
        //int[] arr = new int[]{1, 1, 0, 0, 1};
        //int[] arr = new int[]{1, 1, 0, 0, 1, 0, 3};
        //int[] arr = new int[]{1, 1, 0, 0, 1, 0, 3, 0, 3};
        //int[] arr = new int[]{1, 1, 0, 0, 1, 0, 3, 0, 3, 0, 3};
        //int[] arr = new int[]{1, 1, 0, 0, 1, 0, 3, 0, 3, 0, 3, 0, 3};
        int[] arr = new int[]{1, 2, 0, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8};

        List<IndexGroup> groupList = toGroup(arr);
        // 分组
        groupList.forEach(v -> System.out.println("| Group : " + v));

        System.out.println("========================================================================");
        // 组合方案
        List<List<IndexGroup>> schemeList = analysisScheme(groupList);

        schemeList.forEach(l -> {
            System.out.println("方案：==--------------------------------------------------------------");
            l.forEach(v -> System.out.print("| group: " + v));
            System.out.println("");
        });
        System.out.println("========================================================================");

        List<SchemeVO> allList = new ArrayList<>();
        schemeList.forEach(l -> {
            System.out.println("组合：==--------------------------------------------------------------");
            System.out.println("初始方案：==--------------------------------------------------------------");
            l.forEach(v -> System.out.print("| group: " + v));
            System.out.println();
            List<SchemeVO> list = convert2SchemeVO(l, arr);
            list.forEach(v -> {
                System.out.println("实际方案：==--------------------------------------------------------------");
                System.out.println(v);
            });
            allList.addAll(list);
        });
        System.out.println("group Size: " + groupList.size() + "  方案数: " + allList.size());
    }
}

class SchemeVO {

    public SchemeVO(String code, int[] originalPlanSeq) {
        this.code = code;
        this.originalPlanSeq = originalPlanSeq;
    }

    private String code;

    private int[] originalPlanSeq;

    private int[] optimizedPlanSeq;

    private List<MoveIndexVO> moveList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int[] getOriginalPlanSeq() {
        return originalPlanSeq;
    }

    public void setOriginalPlanSeq(int[] originalPlanSeq) {
        this.originalPlanSeq = originalPlanSeq;
    }

    public int[] getOptimizedPlanSeq() {
        return optimizedPlanSeq;
    }

    public void setOptimizedPlanSeq(int[] optimizedPlanSeq) {
        this.optimizedPlanSeq = optimizedPlanSeq;
    }

    public List<MoveIndexVO> getMoveList() {
        return moveList;
    }

    public void setMoveList(List<MoveIndexVO> moveList) {
        this.moveList = moveList;
    }

    @Override
    public String toString() {
        return "SchemeVO{" +
                "code='" + code + '\'' +
                ", originalPlanSeq=" + Arrays.toString(originalPlanSeq) +
                ", optimizedPlanSeq=" + Arrays.toString(optimizedPlanSeq) +
                ", moveList=" + moveList +
                '}';
    }
}

class MoveIndexVO {

    private Integer originalIndex;

    private Integer targetIndex;

    public MoveIndexVO(Integer originalIndex, Integer targetIndex) {
        this.originalIndex = originalIndex;
        this.targetIndex = targetIndex;
    }

    public Integer getOriginalIndex() {
        return originalIndex;
    }

    public void setOriginalIndex(Integer originalIndex) {
        this.originalIndex = originalIndex;
    }

    public Integer getTargetIndex() {
        return targetIndex;
    }

    public void setTargetIndex(Integer targetIndex) {
        this.targetIndex = targetIndex;
    }

    @Override
    public String toString() {
        return "MoveIndexVO{" +
                "si=" + originalIndex +
                ", ti=" + targetIndex +
                '}';
    }
}

class IndexGroup {
    private int minIdx;
    private int maxIdx;

    public IndexGroup(int minIdx, int maxIdx) {
        this.minIdx = minIdx;
        this.maxIdx = maxIdx;
    }

    public int getMinIdx() {
        return minIdx;
    }

    public void setMinIdx(int minIdx) {
        this.minIdx = minIdx;
    }

    public int getMaxIdx() {
        return maxIdx;
    }

    public void setMaxIdx(int maxIdx) {
        this.maxIdx = maxIdx;
    }

    @Override
    public String toString() {
        return "[" +
                "min=" + minIdx +
                ", max=" + maxIdx +
                ']';
    }
}
