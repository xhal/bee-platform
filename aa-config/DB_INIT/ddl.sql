/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/7/30 上午11:46:42                         */
/*==============================================================*/


drop table if exists t_cfg_database_info;

drop table if exists t_cfg_function_model;

drop table if exists t_cfg_function_model_field;

drop index uk_t_cfg_system_info on t_cfg_system_info;

drop table if exists t_cfg_system_info;

drop table if exists t_cfg_table_column;

drop table if exists t_cfg_table_index;

drop table if exists t_cfg_table_index_column;

drop table if exists t_cfg_table_info;

drop table if exists t_cfg_table_space;

drop index uk_t_sys_user on t_sys_user;

drop table if exists t_sys_user;

drop table if exists t_sys_user_login_log;

/*==============================================================*/
/* Table: t_cfg_database_info                                   */
/*==============================================================*/
create table t_cfg_database_info
(
   tid                  bigint not null comment '表ID',
   connect_name         varchar(200) not null default '0' comment '数据库连接名称',
   db_type              varchar(100) not null comment '数据库类型（mysql/oracle/..)',
   db_url               varchar(300) not null comment '数据库连接URL',
   db_host              varchar(100) comment '服务器地址',
   db_port              varchar(6) comment '服务端口',
   db_schema            varchar(60) comment '数据库实例',
   db_user              varchar(60) not null comment '登陆用户名',
   db_pswd              varchar(60) not null comment '连接密码（加密）',
   pswd_salt            varchar(100) comment '密码盐值',
   data_source_name     varchar(30) comment '程序中的数据源名称标识',
   deploy_type          tinyint default 0 comment '部署类型（0-单主机、10-主从主，11-主从从，21-读写分离读，22读写分享写',
   status_flag          tinyint default 0 comment '状态标识（0-初始创建，1-测试连接成功，2-测试连接异常）',
   remark               varchar(300) comment '备注描述',
   deleted              bigint comment '逻辑删除（0-未删除，等于记录TID则代表已删除）',
   create_by            varchar(32) comment '创建人',
   create_name          varchar(60) comment '创建人名称',
   create_time          datetime comment '创建时间',
   update_by            varchar(32) comment '修改人',
   update_name          varchar(60) comment '修改人名称',
   update_time          datetime comment '修改时间',
   primary key (tid)
);

alter table t_cfg_database_info comment '表中文名：数据库连接信息表
所属模块：配置管理
数据来源：系统内置、本系统录入
用途';

/*==============================================================*/
/* Table: t_cfg_function_model                                  */
/*==============================================================*/
create table t_cfg_function_model
(
   tid                  bigint not null comment '表ID',
   module_id            bigint not null comment '所属功能模块TID',
   model_code           varchar(30) not null comment '模型编码',
   model_name           varchar(60) not null default '0' comment '模型名称',
   status_flag          tinyint not null default 0 comment '状态标记（0-临时状态，1-正式发布）',
   remark               varchar(300) comment '备注说明',
   deleted              bigint comment '逻辑删除（0-未删除，等于记录TID则代表已删除）',
   create_by            varchar(32) comment '创建人',
   create_name          varchar(60) comment '创建人名称',
   create_time          datetime comment '创建时间',
   update_by            varchar(32) comment '修改人',
   update_name          varchar(60) comment '修改人名称',
   update_time          datetime comment '修改时间',
   primary key (tid)
);

alter table t_cfg_function_model comment '表中文名：业务功能数据模型元信息 - 主表
所属模块：配置管理
数据来源：系统内置、本系统录入
';

/*==============================================================*/
/* Table: t_cfg_function_model_field                            */
/*==============================================================*/
create table t_cfg_function_model_field
(
   tid                  bigint not null comment '表ID',
   function_model_id    bigint not null comment '所属功能模块TID',
   field_code           varchar(60) not null comment '字段编码',
   field_name           varchar(60) not null comment '字段名称',
   table_id             bigint comment '目标表对象TID',
   column_id            bigint comment '目标表对象列TID',
   field_order          tinyint not null default 1 comment '字段顺序',
   field_type           tinyint not null default 0 comment '字段类型（0-主表字段，1-拓展表字段，2-字典表值字段，3-虚拟字段）',
   remark               varchar(300) comment '备注说明',
   deleted              bigint not null comment '逻辑删除（0-未删除，等于记录TID则代表已删除）',
   create_by            varchar(32) comment '创建人',
   create_name          varchar(60) comment '创建人名称',
   create_time          datetime comment '创建时间',
   update_by            varchar(32) comment '修改人',
   update_name          varchar(60) comment '修改人名称',
   update_time          datetime comment '修改时间',
   primary key (tid)
);

alter table t_cfg_function_model_field comment '表中文名：业务功能数据模型元信息 - 字段列表
所属模块：配置管理
数据来源：系统内置、本系统录';

/*==============================================================*/
/* Table: t_cfg_system_info                                     */
/*==============================================================*/
create table t_cfg_system_info
(
   tid                  bigint not null comment '表ID',
   module_type          tinyint not null default 0 comment '（0-系统级，1-一级模块，2-二级模块）',
   module_code          varchar(20) not null comment '系统模块编码',
   module_name          varchar(30) not null comment '系统模块名称',
   abridge_code         varchar(20) comment '模块简码',
   introduction         varchar(300) comment '系统简介',
   description          longtext comment '系统详细描述（富文本、Markdown文档内容）',
   data_order           tinyint not null default 1 comment '数据顺序',
   parent_id            bigint not null default 0 comment '父级记录ID',
   deleted              bigint comment '逻辑删除（0-未删除，等于记录TID则代表已删除）',
   create_by            varchar(32) comment '创建人',
   create_name          varchar(60) comment '创建人名称',
   create_time          datetime comment '创建时间',
   update_by            varchar(32) comment '修改人',
   update_name          varchar(60) comment '修改人名称',
   update_time          datetime comment '修改时间',
   primary key (tid)
);

alter table t_cfg_system_info comment '表中文名：系统(模块)信息表
所属模块：配置管理
数据来源：系统内置、本系统录入
用';

/*==============================================================*/
/* Index: uk_t_cfg_system_info                                  */
/*==============================================================*/
create unique index uk_t_cfg_system_info on t_cfg_system_info
(
   module_code,
   deleted
);

/*==============================================================*/
/* Table: t_cfg_table_column                                    */
/*==============================================================*/
create table t_cfg_table_column
(
   tid                  bigint not null comment '表ID',
   table_id             bigint not null comment '表对象ID',
   column_code          varchar(200) not null comment '列名',
   column_name          varchar(200) not null default '0' comment '列显示名称',
   column_comment       varchar(200) not null comment '列注释',
   column_order         tinyint not null default 0 comment '列显示顺序',
   data_type            varchar(200) not null comment '数据类型',
   data_length          varchar(20) comment '数据长度',
   data_percision       varchar(20) not null default 'UTF-8' comment '数据精度（小数位数）',
   default_value        varchar(100) comment '默认值',
   mandatory_flag       varchar(20) not null default '0' comment '是否强制有值（0-否，1-是）',
   pk_flag              tinyint not null default 0 comment '是否为主键标记（0-否，1-是）',
   fk_flag              tinyint not null default 0 comment '是否为外键标记（0-否，1-是）',
   uk_flag              tinyint not null default 0 comment '是否为唯一索引标记（0-否，1-是）',
   idx_flag             tinyint not null default 0 comment '是否为索引标记（0-否，1-是）',
   deleted              bigint comment '逻辑删除（0-未删除，等于记录TID则代表已删除）',
   create_by            varchar(32) comment '创建人',
   create_name          varchar(60) comment '创建人名称',
   create_time          datetime comment '创建时间',
   update_by            varchar(32) comment '修改人',
   update_name          varchar(60) comment '修改人名称',
   update_time          datetime comment '修改时间',
   primary key (tid)
);

alter table t_cfg_table_column comment '表中文名：数据库表对象-字段列元信息
所属模块：配置管理
数据来源：系统内置、本系统录入
                                       -&';

/*==============================================================*/
/* Table: t_cfg_table_index                                     */
/*==============================================================*/
create table t_cfg_table_index
(
   tid                  bigint not null comment '表ID',
   table_id             bigint not null comment '表对象ID',
   index_code           varchar(200) not null comment '索引名（编码）',
   index_type           tinyint not null default 0 comment '索引类型（0-主键索引，1-外键索引，2-唯一索引，3-函数索引，4-普通索引）',
   index_structure      tinyint not null default 0 comment '索引数据结构（0-Btree,1-Bitmap）',
   tablespace_name      varchar(200) comment '表空间名称',
   generation_strategy  tinyint comment '主键生成策略（0-默认-由程序自定义，1-数据库自增，2-雪花ID，3-UUID，4-GUID，5-序列，99-其它））',
   resource_name        varchar(20) comment '资源名称（根据生成策略，存放的值意义不同；如为主键时、生成策略为序列时，此处存放的为序列的名称））',
   status_flag          tinyint not null default 0 comment '状态标记（0-正常，1-只显示关系，不实际创建）',
   remark               varchar(300) comment '备注说明',
   deleted              bigint comment '逻辑删除（0-未删除，等于记录TID则代表已删除）',
   create_by            varchar(32) comment '创建人',
   create_name          varchar(60) comment '创建人名称',
   create_time          datetime comment '创建时间',
   update_by            varchar(32) comment '修改人',
   update_name          varchar(60) comment '修改人名称',
   update_time          datetime comment '修改时间',
   primary key (tid)
);

alter table t_cfg_table_index comment '表中文名：数据库表对象-索引元信息
所属模块：配置管理
数据来源：系统内置、本系统录入
                                      -&#';

/*==============================================================*/
/* Table: t_cfg_table_index_column                              */
/*==============================================================*/
create table t_cfg_table_index_column
(
   tid                  bigint not null comment '表ID',
   index_id             bigint not null comment '索引对象ID',
   column_id            bigint not null comment '索引列对象ID',
   column_code          varchar(200) not null comment '索引列名（函数索引，包含指定函数表达式）',
   foreign_table_id     bigint comment '外键目标表对象id',
   foreign_column_id    bigint comment '外键目标表列id',
   sort_flag            tinyint not null default 0 comment '排序方向（0-升序，1-降序）',
   deleted              bigint not null comment '逻辑删除（0-未删除，等于记录TID则代表已删除）',
   create_by            varchar(32) comment '创建人',
   create_name          varchar(60) comment '创建人名称',
   create_time          datetime comment '创建时间',
   update_by            varchar(32) comment '修改人',
   update_name          varchar(60) comment '修改人名称',
   update_time          datetime comment '修改时间',
   primary key (tid)
);

alter table t_cfg_table_index_column comment '表中文名：数据库表对象-索引列元信息
所属模块：配置管理
数据来源：系统内置、本系统录入
                                             -&';

/*==============================================================*/
/* Table: t_cfg_table_info                                      */
/*==============================================================*/
create table t_cfg_table_info
(
   tid                  bigint not null comment '表ID',
   table_code           varchar(200) not null comment '表名',
   table_name           varchar(200) not null default '0' comment '表显示名称',
   table_comment        varchar(200) not null comment '表注释',
   tablespace_name      varchar(200) not null comment '表空间名称',
   db_engine            varchar(20) comment '数据库存储引擎',
   db_charset           varchar(20) not null default 'UTF-8' comment '字符集（GBK/UTF-8)',
   data_source          varchar(100) comment '数据来源描述（接口同步、文件导入、本系统录入）',
   version_flag         varchar(20) not null default '‘1.0’' comment '版本标记',
   status_flag          tinyint not null default 0 comment '状态标记（0-临时状态，1-正式发布）',
   module_id            bigint comment '模块TID',
   remark               varchar(300) comment '备注描述',
   deleted              bigint comment '逻辑删除（0-未删除，等于记录TID则代表已删除）',
   create_by            varchar(32) comment '创建人',
   create_name          varchar(60) comment '创建人名称',
   create_time          datetime comment '创建时间',
   update_by            varchar(32) comment '修改人',
   update_name          varchar(60) comment '修改人名称',
   update_time          datetime comment '修改时间',
   primary key (tid)
);

alter table t_cfg_table_info comment '表中文名：数据库表对象信息表
所属模块：配置管理
数据来源：系统内置、本系统录入
用';

/*==============================================================*/
/* Table: t_cfg_table_space                                     */
/*==============================================================*/
create table t_cfg_table_space
(
   tid                  bigint not null comment '表ID',
   tablespace_name      varchar(200) not null default '0' comment '表空间名称',
   log_flag             tinyint not null comment '是否启用日志（0-否，1-是）',
   data_file_path       varchar(300) not null comment '文件路径',
   initial_size         int comment '初始化文件大小（MB）',
   file_block_size      int not null comment '文件数据块大小（MB）',
   max_size             int not null comment '最大文件大小（MB）',
   auto_extent_flag     tinyint comment '是否自动拓展空间（0-否，1-是）',
   extent_size          int comment '自动拓展空间大小',
   remark               varchar(300) comment '备注描述',
   deleted              bigint comment '逻辑删除（0-未删除，等于记录TID则代表已删除）',
   create_by            varchar(32) comment '创建人',
   create_name          varchar(60) comment '创建人名称',
   create_time          datetime comment '创建时间',
   update_by            varchar(32) comment '修改人',
   update_name          varchar(60) comment '修改人名称',
   update_time          datetime comment '修改时间',
   primary key (tid)
);

alter table t_cfg_table_space comment '表中文名：数据库表空间信息表
所属模块：配置管理
数据来源：系统内置、本系统录入
用';

/*==============================================================*/
/* Table: t_sys_user                                            */
/*==============================================================*/
create table t_sys_user
(
   tid                  bigint not null comment '表ID',
   user_account         varchar(32) not null comment '用户账号',
   user_pswd            varchar(32) not null comment '密码',
   pswd_salt            varchar(60) not null comment '密码加密盐值',
   nick_name            varchar(20) not null comment '昵称',
   real_name            varchar(60) not null comment '真实姓名',
   gender               tinyint not null default 1 comment '性别（0-女，1-男）',
   birthday             date comment '生日',
   email                varchar(60) comment '电子邮箱地址',
   cell_phone_number    varchar(20) comment '手机号码',
   phone_number         varchar(20) comment '座机号',
   login_count          int default 0 comment '登陆次数',
   last_login_time      datetime comment '最后登陆时间',
   last_login_ip        varchar(32) comment '最后登陆IP',
   status_flag          tinyint default 0 comment '状态标识（0-正常，1-限制登陆，2-禁用',
   deleted              bigint comment '逻辑删除（0-未删除，等于记录TID则代表已删除）',
   create_by            varchar(32) comment '创建人',
   create_name          varchar(60) comment '创建人名称',
   create_time          datetime comment '创建时间',
   update_by            varchar(32) comment '修改人',
   update_name          varchar(60) comment '修改人名称',
   update_time          datetime comment '修改时间',
   primary key (tid)
);

alter table t_sys_user comment '系统用户信息表';

/*==============================================================*/
/* Index: uk_t_sys_user                                         */
/*==============================================================*/
create unique index uk_t_sys_user on t_sys_user
(
   user_account,
   deleted
);

/*==============================================================*/
/* Table: t_sys_user_login_log                                  */
/*==============================================================*/
create table t_sys_user_login_log
(
   tid                  bigint not null comment '表ID',
   user_id              bigint not null comment '用户表TID',
   login_count          int not null default 0 comment '登陆次数',
   login_time           datetime not null comment '登陆时间',
   login_ip             varchar(32) comment '登陆IP',
   system_type          varchar(36) comment '系统类型(Window/ Linux/ MacOs /Andiord/ ISO等等)',
   browser_type         varchar(36) comment '浏览器类型（Chrome/IE/WindowsEdge等等）',
   status_flag          tinyint not null default 0 comment '登陆状态标识（0-正常登陆，1-密码错误'
);

alter table t_sys_user_login_log comment '系统用户登陆日志表';

