package cc.xhal.platform.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 功能说明：Spring Boot 启动类
 *
 * @author hal 2021/7/28 22:23
 * @version 1.0
 */
@SpringBootApplication
@ComponentScan(basePackages = {"cc.xhal"})
public class PlatformApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(PlatformApplication.class, args);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {

        return new WebMvcConfigurerAdapter() {
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        //.allowedOrigins(ip)  //可访问ip，ip最好从配置文件中获取，
                        .allowedMethods("PUT", "DELETE", "GET", "POST").allowedHeaders("*")
                        .exposedHeaders("access-control-allow-headers", "access-control-allow-methods", "access-control-allow-origin", "access-control-max-age", "X-Frame-Options")
                        .allowCredentials(false).maxAge(3600);
            }
        };

    }

}
