package cc.xhal.platform.boot.base.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * 功能说明：首页控制层
 *
 * @author hal 2021/7/30 18:10
 * @version 1.0
 */
@Controller
public class IndexController {

    /**
     * 首页
     *
     * @return String
     */
    @ResponseBody
    @RequestMapping(value = {"", "/", "/index"})
    public String index() {
        return "this is Index page!!! time: " + new Date();
    }
}
